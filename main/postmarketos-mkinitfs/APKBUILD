# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
pkgname=postmarketos-mkinitfs
pkgver=0.25
pkgrel=1
pkgdesc="Tool to generate initramfs images for postmarketOS"
url="https://postmarketos.org"
depends="
	busybox-extras
	bzip2
	multipath-tools
	cryptsetup
	device-mapper
	e2fsprogs
	e2fsprogs-extra
	f2fs-tools
	kmod
	lddtree
	lz4
	osk-sdl>=0.64
	parted
	xz
	"
replaces="mkinitfs"
triggers="$pkgname.trigger=/etc/postmarketos-mkinitfs/hooks:/usr/share/kernel/*:/usr/share/postmarketos-mkinitfs-triggers"
source="00-default.modules
	init.sh.in
	init_functions.sh
	mkinitfs.sh
	mkinitfs_functions.sh
	mkinitfs_test.sh
	"
arch="noarch"
license="GPL-2.0-or-later"
provides="mkinitfs=0.0.1"

package() {
	for file in init.sh.in init_functions.sh mkinitfs_functions.sh; do
		install -Dm644 "$srcdir/$file" \
			"$pkgdir/usr/share/postmarketos-mkinitfs/$file"
	done

	install -Dm644 "$srcdir/00-default.modules" \
		"$pkgdir/etc/postmarketos-mkinitfs/modules/00-default.modules"

	install -Dm755 "$srcdir/mkinitfs.sh" \
		"$pkgdir/sbin/mkinitfs"

	mkdir -p "$pkgdir/etc/postmarketos-mkinitfs/hooks/"
}

check() {
	/bin/busybox sh ./mkinitfs_test.sh
}
sha512sums="
4717bf24bd13fd4d90f3ab000ab43f7d61529515de224ebc86458ae709ebe0b5065655457e34f5952126bac6ab45143a91cddb09a8ad2d0a55c5cecd564a0135  00-default.modules
1a267dd958609263a4f24657f602ac0e4e0c724bf1284adec85e82d05c9b07553085edea5a56f7fa09d1652af232c72d4d39a06858c2a580ff830577b5f37d67  init.sh.in
e72adfb06745ad596a02837c8d52837490a7940f4d5589b8937155ee1be1bc4176d79d3f24c40882d20592d00c23c2daf3ef1ceba6ef05cadb76906084373f1b  init_functions.sh
4f74995c52b3fd18ef77347dbbca4b3575d23fd339b62a257c1c4b45a16feb5a95f391d6189f62e94ac00d8da3077d192c678554a5f9b8190e34bf3fa6c86e95  mkinitfs.sh
93dfd3516870d388d5d124c162c43a327aa716eeabbc8cbed3378711e9347ef76919c09dac051f833230caff7607008d4c38022cc1d910c7392012e22818e890  mkinitfs_functions.sh
c7a3c33daeb12b33ac72207191941c4d634f15c22958273b52af381a70ebaba1d3a9299483f0c447d9e66c560151fe7b9588bb4bbef2c8914f83185984ee4622  mkinitfs_test.sh
"
